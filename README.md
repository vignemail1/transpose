# transpose

A tool to transpose some CSV like input with customizable input and output separator (default separators: ",")

```bash
$ cat file.txt
test,1,2,3
4,5,6,7

$ cat file.txt | ./transpose "," "|"
test|4
1|5
2|6
3|7
```

## Download

Go to <https://gitlab.com/vignemail1/transpose/-/packages> and choose one of the available version (currently: linux_amd64, darwin_amd64 and darwin_arm64 [for Apple M1 processor])
