package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	args := os.Args
	separatorIn := ","
	separatorOut := ","
	if (len(args)) == 2 {
		separatorIn = args[1]
		separatorOut = args[1]
	}
	if (len(args)) >= 3 {
		separatorIn = args[1]
		separatorOut = args[2]
	}
	reader := bufio.NewReader(os.Stdin)
	sample := [][]string{}
	for {
		str, err := reader.ReadString('\n')

		if err == io.EOF {
			// fmt.Println("EOF")
			break
		}
		if err != nil {
			log.Fatal(err)
			break
		}
		str = strings.TrimSuffix(str, "\n")
		tab := strings.Split(str, separatorIn)
		sample2 := make([][]string, len(sample)+1)
		sample2 = append(sample, tab)
		sample = sample2
		// fmt.Println("loop")
	}
	// fmt.Println("after loop", sample)
	ar := transpose(sample)
	for _, line := range ar {
		line := strings.Join(line, separatorOut)
		fmt.Println(line)
	}
	// fmt.Println(ar)
}

func transpose(slice [][]string) [][]string {
	xl := len(slice[0])
	yl := len(slice)
	result := make([][]string, xl)
	for i := range result {
		result[i] = make([]string, yl)
	}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			result[i][j] = slice[j][i]
		}
	}
	return result
}
